# WeekOf
=========

Simple moment.js plugin to support a custom way of reckoning which week a day belongs to. In this scheme all weeks start on Monday. This also provides the ability to get an array of moment objects that match each Monday in the year.

## Installation

  `npm install moment`
  `npm install weekof`

## Usage

    // the moment object will be for the start of January 1, 2018
    let jan1_2018 = moment.utc('2018-01-01').weekof()

    // this moment object will be for the start of Decemeber 31, 2018
    let jan1_2019 = moment.utc('2019-01-01').weekof()

    //get an array of moment objects for the beginning of every week in 2018
    let mondays_2018 = moment.weekof_mondays(2018)

## Tests

  `npm test`