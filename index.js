
moment = require('moment')

moment.fn.weekof = function() {
  //returns a new moment object for the monday this moment is the week of
  var monday = this.clone().isoWeekday(1)
  return monday.startOf('day')
};

moment.weekof_mondays = function(year) {
  //return an array of moment objects, one for each monday in the given year
  var mondays = []
  var week = moment({year: year}).weekof()
  if (week.year() < parseInt(year,10)) {
    //move to first monday of the year
    week.add(1, 'week')
  }

  while(week.year() == parseInt(year, 10)) {    
    mondays.push(week)
    week = week.clone().add(1, 'week')
  }
  return mondays
}

module.exports = moment