'use strict';

const expect = require('chai').expect;
const moment = require('moment')
const { weekof } = require('../index.js');

describe('weekof', function() {

    it('should correctly calculate start of week on year boundaries', function() {

        let jan1_2018 = moment.utc('2018-01-01').weekof()
        expect(jan1_2018.year()).to.equal(2018)
        expect(jan1_2018.date()).to.equal(1)

        let dec31_2018 = moment.utc('2018-12-31').weekof()
        expect(dec31_2018.year()).to.equal(2018)
        expect(dec31_2018.date()).to.equal(31)

        let jan1_2019 = moment.utc('2019-01-01').weekof()
        expect(jan1_2019.year()).to.equal(2018)
        expect(jan1_2019.date()).to.equal(31)

        let jan6_2019 = moment.utc('2019-01-06').weekof()
        expect(jan6_2019.year()).to.equal(2018)
        expect(jan6_2019.date()).to.equal(31)

        let jan7_2019 = moment.utc('2019-01-07').weekof()
        expect(jan7_2019.year()).to.equal(2019)
        expect(jan7_2019.date()).to.equal(7)

    });

    it('should calculate all the mondays for 2018', function() {
        let mondays_2018 = moment.weekof_mondays(2018)
        let length = mondays_2018.length
        expect(length).to.equal(53)
        expect(mondays_2018[0].year()).to.equal(2018)
        expect(mondays_2018[0].date()).to.equal(1)

        expect(mondays_2018[length - 1].year()).to.equal(2018)
        expect(mondays_2018[length - 1].month()).to.equal(11)
        expect(mondays_2018[length - 1].date()).to.equal(31)
    })

    it('should calculate all the mondays for 2019', function() {
        let mondays_2019 = moment.weekof_mondays(2019)
        let length = mondays_2019.length
        expect(length).to.equal(52)
        expect(mondays_2019[0].year()).to.equal(2019)
        expect(mondays_2019[0].date()).to.equal(7)

        expect(mondays_2019[length - 1].year()).to.equal(2019)
        expect(mondays_2019[length - 1].month()).to.equal(11)
        expect(mondays_2019[length - 1].date()).to.equal(30)
    })

    it('should allow me to lookup a monday of given week number', function() {
        let mondays_2019 = moment.weekof_mondays(2019)
        let week_3 = mondays_2019[2]
        expect(week_3.month()).to.equal(0)
        expect(week_3.date()).to.equal(21)

        let week_52 = mondays_2019[51]
        expect(week_52.month()).to.equal(11)
        expect(week_52.date()).to.equal(30)

    })
})